#!/bin/sh
/etc/init.d/dbus start
sleep 5
PASS=${PASS:-$(pwgen -s 6 1)}
echo "user:$PASS" | chpasswd
/etc/NX/nxserver --startup
tail -F /usr/NX/var/log/nxserver.log
