docker kill desk
docker rm desk
docker run -d \
    -p 4000:4000 \
    -p 4683:4683/udp \
    -p 4213:4213 \
    -p 222:22 \
    -e PASS=test123 \
    --cap-add=SYS_ADMIN \
    --cap-add=SYS_PTRACE \
    --privileged \
    --shm-size 1024m \
    --name desk \
    nxdesk:ubuntu-kde
