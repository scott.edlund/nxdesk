FROM ubuntu:21.10

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update \
    && apt upgrade -y \
    && apt install -y --no-install-recommends sudo less pwgen wget pwgen pulseaudio kubuntu-desktop konsole dbus-x11

ENV NOMACHINE_URL https://download.nomachine.com/download/7.6/Linux/nomachine_7.6.2_4_amd64.deb 
ENV NOMACHINE_MD5 4fffc2d252868086610b0264c30461bd

RUN apt install -y curl libappindicator3-1 fonts-liberation xdg-utils lsb-release \
    && curl -fSL "${NOMACHINE_URL}" -o nomachine.deb \
    && echo "${NOMACHINE_MD5} *nomachine.deb" | md5sum -c - \
    && dpkg -i nomachine.deb \
    && rm nomachine.deb \
    && groupadd -r user -g 433 \
    && useradd -u 431 -r -g user -d /home/user -s /bin/bash -c "user" user \
    && usermod -aG sudo user \
    && mkdir /home/user \
    && chown -R user:user /home/user \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && echo 'user:user' | chpasswd

RUN apt install -y apt-transport-https curl gnupg \
    && curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add - \
    && echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list \
    && apt update \
    && apt install -y brave-browser

RUN apt clean

ADD nxserver.sh /

ENTRYPOINT ["/nxserver.sh"]
